const express = require('express');
const router = express.Router();
const Cars = require('../models/cars');

router.post('/addcar', (req, res) => {
    console.log(req.body);
    let newCar = new Cars({
        model: req.body.modelName,
        submodel: [
            {
                name: req.body.submodel1name,
                imageUrl: req.body.submodel1imageUrl
            },
            {
                name: req.body.submodel2name,
                imageUrl: req.body.submodel2imageUrl
            }
            ]
    });

    newCar.save((err, car) => {
        if(err) {
            res.json({success: false, message: "Something went wrong"});
        } else {
            res.json({success: true, message: "Saved Successfully"});
        }
    });
});

router.post('/getacar', (req, res) => {
    console.log(req.body);
    Cars.find({model: req.body.modelName}, (err, car) => {
        if(err) {
            res.json({message: "No car available with this model name"});
        } else {
            console.log(car.length);
            res.json(car);
        }
    })
});

router.get('/getallcars', (req, res) => {
    Cars.find({}, (err, car) => {
        if(err) {
            res.json({message: "Something went wrong"});
        } else {
            res.json(car);
        }
    })
});

module.exports = router;