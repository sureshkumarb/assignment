# Steps to run #

# Step 1: Clone this repository #
git clone https://sureshkumarb@bitbucket.org/sureshkumarb/assignment.git

# Step 2: Go to Root Directory and installing packages #
run "npm install"

# Step 3: Run Server #
Before running "npm start" make sure mongodb service is running in the background.
Then run "npm start"

# Step 4: Listen at port 3000 #
Go to localhost:3000 is your browser and see the output
