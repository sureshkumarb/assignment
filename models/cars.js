const mongoose = require('mongoose');

// Car Schema
const CarSchema = mongoose.Schema({
    model : {
        type: String
    },
    submodel : {
        type: Array
    }
});

module.exports = mongoose.model('Car', CarSchema);